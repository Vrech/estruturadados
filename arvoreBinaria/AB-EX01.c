#include <stdio.h>
#include <stdlib.h>

struct node
{
    int ra;
    int mensalidade;
    struct node* left;
    struct node* right;
};

struct node* novoNo(int ra)
{
  struct node* node = (struct node*) malloc(sizeof(struct node));

  node->ra = ra;
  node->left  = NULL;
  node->right = NULL;

  return(node);
}


struct node* inserir(struct node* node, int ra, int mensalidade)
{
  if (node == NULL)
    return(novoNo(ra));
  else
  {

    //recursividade
    if (ra <= node->ra)
        node->left  = inserir(node->left, ra, mensalidade);
    else
        node->right = inserir(node->right, ra, mensalidade);

    return node;
  }
}

void imprimirAluno(struct node* node) {
	if (node != NULL) {
		printf("Aluno localizado RA: %d \n", node->ra);
	} else {
		printf("Aluno nao localizado! \n");
	}
}

struct node* pesquisar(struct node* node, int ra) {

  if(!node || ra == node->ra) {
    return node;

  }else if(ra < node->ra){
    return pesquisar(node->left, ra);

  } else {
    return pesquisar(node->right, ra);
  }
}

void ordem(struct node* node) {
  if(node == NULL)
    return;

  if(node != NULL) {
    ordem(node->left);
    printf("%d \n", node->ra);
    ordem(node->right);
  }
}

void posOrdem(struct node* node) {
  if(node == NULL)
    return;

  if(node != NULL) {
    posOrdem(node->left);
    posOrdem(node->right);
    printf("%d \n", node->ra);
  }
}

void preOrdem(struct node* node) {
  if(node == NULL)
    return;

  if(node != NULL) {
    printf("%d \n", node->ra);
    preOrdem(node->left);
    preOrdem(node->right);
  }
}

void liberaNo(struct node* node) {
  if(node == NULL)
    return;

  liberaNo(node->left);
  liberaNo(node->right);
  free(node);
  node = NULL;
}

void esvaziarArvore(struct node* node) {
  if(node == NULL)
    return;

  liberaNo(node);
  free(node);
}


int main(int argc, char *argv[]) {
	int opc = 0;
	int valor = 0;

	struct node* root = NULL;

	while(opc != 13) {
		printf(" \n");
		printf(":: ARVORE BINARIA ::.. \n");
		printf("1. Inserir\n");
		printf("2. Pesquisar (RA) \n");
		printf("3. Imprimir em Ordem \n");
    printf("4. Imprimir em Pós-Ordem \n");
    printf("5. Imprimir em Pré-Ordem \n");
    printf("6. Excluir (RA) \n");
    printf("7. Esvaziar árvore \n");
    printf("8. Contar número de folhas \n");
    printf("9. Contar número de nós com grau X \n");
    printf("10. Exibir o grau do nó (ra) \n");
    printf("11. Exibir todas as informações dos nós folhas \n");
    printf("12. Exibir todos os nós com mensalidade superior valor X qualquer \n");
		printf("13. sair\n");
		printf("Dig. opcao desejada: \n");
		scanf("%d", &opc);

		if (opc == 1) {
      int ra;
      int mensalidade;

      printf("Digite o RA. \n");
      scanf("%d", &ra);

      printf("Digite a Mensalidade. \n");
      scanf("%d", &mensalidade);

			root = inserir(root, ra, mensalidade);

		} else if(opc == 2) {
      int ra;

      printf("Digite o RA. \n");
      scanf("%d", &ra);

      root = pesquisar(root, ra);
      imprimirAluno(root);

    } else if(opc == 3) {
      ordem(root);

    } else if(opc == 4) {
      posOrdem(root);

    } else if(opc == 5) {
      preOrdem(root);

    }else if(opc == 6) {

    }else if(opc == 7) {
      esvaziarArvore(root);
    }
	}

	return 0;
}
