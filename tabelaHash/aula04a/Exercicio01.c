// :: EXER01 - Painel de atendimento de pastelaria ::..
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define TAMANHO 20

struct Pedido {
   int numero;
   char* nome;
   int horario;
};

struct Pedido* hashVetor[TAMANHO];
struct Pedido* modeloPedido;
struct Pedido* pedido;

int hashCodigo(int chave) {
   return chave % TAMANHO;
}

struct Pedido *pesquisar(int chave) {
   //obtem o c�digo hash
   int hashIndex = hashCodigo(chave);

   if(hashVetor[hashIndex] != NULL){
     return 1;
   }else{
     return 0;
   }

   printf("in: %d\n", hashIndex);
   //percorre o vetor enquanto estiver vazio
   while(hashVetor[hashIndex] != NULL) {

      if(hashVetor[hashIndex]->numero == chave){
      	return hashVetor[hashIndex];
	    }
      printf("ak: %d\n", hashVetor[hashIndex]);
      //vai para pr�xima celula
      ++hashIndex;

      //wrap around the table
      hashIndex %= TAMANHO;
   }

   return NULL;
}

void inserir(int numero, char* nome, int horario) {
   struct Pedido *pedido = (struct Pedido*) malloc(sizeof(struct Pedido));

printf("boolean: %d", pesquisar(1));

   if(pesquisar(1)) {
     printf("Número já existente!");
   }else {

     pedido->numero = numero;
     pedido->nome = nome;
     pedido->horario = horario;

     int hashIndex = hashCodigo(numero);

     while(hashVetor[hashIndex] != NULL && hashVetor[hashIndex]->numero != -1) {
        ++hashIndex;
        hashIndex %= TAMANHO;
     }

     hashVetor[hashIndex] = pedido;
   }
}

void imprimirTodosItensVetor() {
   int i = 0;

   for(i = 0; i < TAMANHO; i++) {
      if(hashVetor[i] != NULL) {
      	printf("%i - (%i, %s, %i) \n", i, hashVetor[i]->numero, hashVetor[i]->nome, hashVetor[i]->horario);
	  }
   }

   printf("\n");
}

int main(int argc, char *argv[]) {
  int opc = 0;

  modeloPedido = (struct Pedido*) malloc(sizeof(struct Pedido));
	modeloPedido->numero = -1;
	modeloPedido->nome = "";
	modeloPedido->horario = -1;

	while(opc != 9) {
		printf(" \n");
		printf(":: Painel de Atendimento de Pastelaria ::.. \n");
		printf("1. Inserir pedido \n");
		printf("2. Listar Pedidos \n");
		printf("3. Localizar valor \n");
		printf("4. Remover valor \n");
		printf("9. sair\n");
		printf("Dig. opcao desejada: \n");
		scanf("%d", &opc);

    if(opc == 1) {
      char* nome;
      int horario;
      int numero;

			printf("Digite o seu nome. \n");
			scanf("%s", nome);

      printf("Digite o horário. \n");
			scanf("%d", &horario);

      numero = rand() % 1000;

      inserir(numero, nome, horario);

    } else if(opc == 2) {
      imprimirTodosItensVetor();
    }
  }

    return 0;
}
