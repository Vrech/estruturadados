#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define TAMANHO 6

struct Aluno {
   int ra;
   int idade;
};

struct Aluno* hashVetor[TAMANHO];
struct Aluno* modeloAluno;
struct Aluno* Aluno;


int hashCodigo(int chave) {
   return chave % TAMANHO;
}

struct Aluno *pesquisar(int chave) {
   //obtem o c�digo hash
   int hashIndex = hashCodigo(chave);

   printf("in: %d\n", hashIndex);
   //percorre o vetor enquanto estiver vazio
   while(hashVetor[hashIndex] != NULL) {

      if(hashVetor[hashIndex]->ra == chave){
      	return hashVetor[hashIndex];
	    }
      //vai para pr�xima celula
      ++hashIndex;

      //wrap around the table
      hashIndex %= TAMANHO;
   }

   return NULL;
}

void imprimirAluno(struct Aluno* Aluno) {
	if (Aluno != NULL) {
		printf("Aluno localizado RA: %d \n", Aluno->ra);
	} else {
		printf("Aluno nao localizado! \n");
	}
}

void inserir(int ra, int idade) {
   struct Aluno *Aluno = (struct Aluno*) malloc(sizeof(struct Aluno));

     Aluno->ra = ra;
     Aluno->idade = idade;

     int hashIndex = hashCodigo(ra);

     while(hashVetor[hashIndex] != NULL && hashVetor[hashIndex]->ra != -1) {
        ++hashIndex;
        hashIndex %= TAMANHO;
     }

     hashVetor[hashIndex] = Aluno;
}

void imprimirTodosItensVetor() {
   int i = 0;

   for(i = 0; i < TAMANHO; i++) {
      if(hashVetor[i] != NULL) {
      	printf("%i - (%i, %i) \n", i, hashVetor[i]->ra, hashVetor[i]->idade);
	  }
   }

   printf("\n");
}

struct Aluno* remover(struct Aluno* Aluno) {
   int chave = Aluno->ra;

   //obtem codigo hash
   int hashIndex = hashCodigo(chave);

   //percorre o vetor enquanto estiver vazio
   while(hashVetor[hashIndex] != NULL) {

      if(hashVetor[hashIndex]->ra == chave) {
         struct Aluno* temp = hashVetor[hashIndex];

         //atribui o Aluno modelo a posicao deletada
         hashVetor[hashIndex] = modeloAluno;
         return temp;
      }

      //para pr�xima posicao
      ++hashIndex;

      //wrap around the table
      hashIndex %= TAMANHO;
   }

   return NULL;
}

int main(int argc, char *argv[]) {
  int opc = 0;

  modeloAluno = (struct Aluno*) malloc(sizeof(struct Aluno));
	modeloAluno->ra = -1;
	modeloAluno->idade = -1;

	while(opc != 9) {
		printf(" \n");
		printf(":: Cadastro de alunos para palestra ::.. \n");
		printf("1. Inserir Aluno \n");
		printf("2. Listar Alunos \n");
		printf("3. Localizar Aluno \n");
		printf("4. Remover Aluno \n");
		printf("9. sair\n");
		printf("Dig. opcao desejada: \n");
		scanf("%d", &opc);

    if(opc == 1) {
      int ra;
      int idade;

      printf("Digite o RA. \n");
			scanf("%d", &ra);

      printf("Digite a idade. \n");
			scanf("%d", &idade);

      Aluno = pesquisar(ra);

       if(Aluno != NULL) {
          printf("Aluno já cadastrado");

      } else {
        inserir(ra, idade);
      }

    } else if(opc == 2) {
      imprimirTodosItensVetor();

    } else if(opc == 3) {
      int chave;
			int valor = 0;

			printf("Digite o RA para localizar o aluno: \n");
			scanf("%d", &chave);

			Aluno = pesquisar(chave);
			imprimirAluno(Aluno);

    }else if(opc == 4) {
      int chave;
      int valor = 0;

      printf("Digite o RA para remover o aluno: \n");
      scanf("%d", &chave);

      Aluno = pesquisar(chave);
      remover(Aluno);
    }
  }

    return 0;
}
