#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

int main(int argc, char *argv[]) {
  int opc;
  int qtd;
  float valor;
  float valorTotal;
  float troco;

  while(opc != 9) {
		printf(" \n");
		printf(":: Painel Feira ::.. \n");
		printf("1. Pera \n");
		printf("2. Alface \n");
		printf("3. Laranja\n");
		printf("4. Nabo\n");
    printf("5. Fechar Conta\n");
		printf("Dig. opcao desejada: \n");
		scanf("%d", &opc);

    if(opc == 1) {
      printf("Digite a quantidade. \n");
      scanf("%d", &qtd);

      if(qtd <= 5) {
        valorTotal = qtd * 1.50;
      } else {
        valorTotal = qtd * 1.20;
      }

    } else if(opc == 2) {
      printf("Digite a quantidade. \n");
      scanf("%d", &qtd);

      if(qtd <= 2) {
        valorTotal += qtd * 2.00;
      } else {
        valorTotal += qtd * 1.50;
      }
    } else if(opc == 3) {
      printf("Digite a quantidade. \n");
      scanf("%d", &qtd);

      if(qtd <= 6) {
        valorTotal += qtd * 0.50;
      } else {
        valorTotal += qtd * 0.40;
      }
    } else if(opc == 4) {
      printf("Digite a quantidade. \n");
      scanf("%d", &qtd);

      valorTotal += qtd * 1.50;
    } else {
      printf("Digite o valor. \n");
      scanf("%f", &valor);

      if(valor > valorTotal) {
        troco = valor - valorTotal;
      }

      printf("O valor total é de: %.2f \n", valorTotal);
      printf("O troco é de: %.2f", troco);
    }
  }
}
